import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import io.restassured.parsing.Parser;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.List;

public class RESTAssuredJSONTests {
    final static String ROOT_URI = "http://localhost:7000";

    static {
        RestAssured.defaultParser = Parser.JSON;
    }

    @Test(priority = 1)
    public void get_all_products_test() {
        Response response = get(ROOT_URI + "/products");
        response.then().statusCode(200);

        response.then().body("size()", Matchers.is(10));

        response.then().body("name", Matchers.hasItems(
                "Laptop", "Smartphone", "Coffee Maker",
                "Running Shoes", "Headphones", "Dumbbell Set",
                "Backpack", "Digital Camera", "Gaming Console",
                "Yoga Mat"));
    }

    @Test(priority = 2)
    public void post_product_test() {
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"id\": \"11\", \"name\": \"Tablet\", \"price\": \"300\", \"category\": \"Electronics\"}")
                .when()
                .post(ROOT_URI + "/products");

        response.then().statusCode(201);
        response.then().body("name", Matchers.is("Tablet"));
        response.then().body("price", Matchers.is("300"));
        response.then().body("category", Matchers.is("Electronics"));
        response.then().body("id", Matchers.is("11"));
    }

    @DataProvider
    public Object[][] dpGetWithParam() {
        return new Object[][] {
                {"{\"id\": \"12\", \"name\": \"Smartwatch\", \"price\": \"200\", \"category\": \"Electronics\"}", 201},
                {"{\"id\": \"13\", \"name\": \"Sneakers\", \"price\": \"80\", \"category\": \"Footwear\"}", 201},
                {"{\"id\": \"14\", \"name\": \"T-shirt\", \"price\": \"20\", \"category\": \"Clothing\"}", 201},
        };
    }

    @Test(dataProvider = "dpGetWithParam", priority = 3)
    public void post_with_dp_test(String requestBody, int expectedStatusCode) {
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(ROOT_URI + "/products");

        response.then().statusCode(expectedStatusCode);

        response.then().body("id", Matchers.notNullValue());
        response.then().body("name", Matchers.notNullValue());
        response.then().body("price", Matchers.notNullValue());
        response.then().body("category", Matchers.notNullValue());
    }

    @Test(priority = 4)
    public void put_test() {
        String productId = given()
                .get(ROOT_URI + "/products")
                .then()
                .statusCode(200)
                .extract()
                .path("find { it.name == 'Tablet' }.id");

        Assert.assertNotNull(productId, "ID for product named \"Tablet\" not found.");

        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"id\": \"" + productId + "\", \"name\": \"Tablet 2.0\", \"price\": \"400\", \"category\": \"Electronics\"}")
                .when()
                .put(ROOT_URI + "/products/" + productId);

        response.then().statusCode(200);

        response = get(ROOT_URI + "/products/" + productId);

        response.then().body("name", Matchers.is("Tablet 2.0"));
        response.then().body("price", Matchers.is("400"));
    }

    @Test(priority = 5)
    public void delete_products_by_category_test() {
        Response allFitnessProductsResponse = get(ROOT_URI + "/products?category=Fitness Equipment");
        allFitnessProductsResponse.then().statusCode(200);

        List<String> productIdsToDelete = allFitnessProductsResponse.jsonPath().getList("id");

        for (String productId : productIdsToDelete) {
            Response deleteResponse = delete(ROOT_URI + "/products/" + productId);
            deleteResponse.then().statusCode(200);
        }

        Response getAllFitnessProductsResponse = get(ROOT_URI + "/products?category=Fitness Equipment");
        getAllFitnessProductsResponse.then().statusCode(200);

        Assert.assertTrue(getAllFitnessProductsResponse.jsonPath().getList("$").isEmpty(), "Not all products with category 'Fitness Equipment' have been deleted.");
    }
}
