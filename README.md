Cilj ovog projekta je automatsko testiranje REST API-ja Web aplikacije.

Alati koristeni u ovom projektu:<br />
&nbsp;&nbsp;&nbsp;&nbsp;JAVA JDK<br />
&nbsp;&nbsp;&nbsp;&nbsp;TestNG<br />
&nbsp;&nbsp;&nbsp;&nbsp;IntelliJ IDEA<br />
&nbsp;&nbsp;&nbsp;&nbsp;Maven<br />
&nbsp;&nbsp;&nbsp;&nbsp;Json Server<br />

Baza podataka koja je bila koristena je "db.json" i u njoj se nalazi 10 razlicitih proizvoda.

Izvedeno je 7 testnih metoda:<br />
&nbsp;&nbsp;&nbsp;&nbsp;1. get_all_products_test - dohvaca sve proizvode, provjerava ima li ih 10 i provjerava po nazivima<br />
&nbsp;&nbsp;&nbsp;&nbsp;2. post_product_test - dodaje 1 proizvod te provjerava je li on stvarno dodan<br />
&nbsp;&nbsp;&nbsp;&nbsp;3. post_with_dp_test - koristi DataProvider i metodu dpGetWithParam kako bi se pozvao 3 puta s razlicitim parametrima; dodaje vise proizvoda i provjerava da nisu null<br />
&nbsp;&nbsp;&nbsp;&nbsp;4. put_test - pronalazi proizvod po nazivu i mijenja mu podatke te provjerava jesu li se stvarno promijenili<br />
&nbsp;&nbsp;&nbsp;&nbsp;5. delete_products_by_category_test - dohvaca sve proizvode jedne kategorije i sve ih brise te provjerava jesu li stvarno obrisani

Nakon sto su izvedene sve metode, generirano je izvjesce koje se nalazi u "target\surefire-reports\index.html" u kojem se nalaze svi detalji. 
